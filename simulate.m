function [A, b, x0, delta] = simulate(n)
% [A, b, x0, delta] = simulate(n), zgenerira tako matriko A in tak
% stolpicni vektor b, za katere velja da je stacionarna tocka pozitivna in
% narise graf za n korakov
% vrne matriko A, vektor b, zacetni priblizek x0, in delta, ki je razlika
% med x0 in stacionarno točko sistema

coefA = 0.1;
coefB = 1;
coefX = 0.1;

    mask = [ 
             0, 1, 1, 0, 0, 0;
            -1, 0, 0, 1, 0, 0;
            -1, 0, 0, 1, 1, 0;
             0,-1,-1, 0, 0, 1;
             0, 0,-1, 0, 0, 1;
             0, 0, 0,-1,-1, 0
           ];

    while 1
        % generiramo 6x6 random matriko z masko mask
        A = rand(6).*mask*coefA;

        % generiramo random 6x1 stolpicni vektor
        b = [-0.1 -0.1 -0.1 -0.1 -0.1 0.1]'.* rand(6,1)*coefB;

        % iscemo samo pozitivne stacionarne tocke
        x0 = A\-b;
        if ~sum(x0<0) && Jacobi(A, b, x0)
            break;
        end
    end

    % izracunamo stacionarno tocko in jo malce pokvarimo
    x0 = x0 + rand*coefX;

    % razlika med stacionarno tocko, in našo zacetno vrednostjo
    delta = A\-b - x0; 

    % funkcija sistema
    f = @(X) X.*(b + A*X);
    
    save('out.txt', 'A', 'b', 'x0', 'delta');
    
    % rk4
    x = x0;
    X = [x];
    h = 0.05;
    for i = 1:n
       k1 = f(x);
       k2 = f(x+k1/2);
       k3 = f(x+k2/2);
       k4 = f(x+k3);
       x = x + h*(k1+k2+k2+k3+k3+k4)/6;
       X = [X x];
    end

    % plot
    clf
    hold on
    for i = 1:6
        plot(X(i,:));
    end
end


% Jacobian
% izracuna Jacobijevo matriko sistema, in vrne nazaj 0 ali 1,0 ce je sistem
% asimptoticno unstable in 1 ce je sistem asimptoticno stable
function [isStable] = Jacobi(A, b, X)
    J=zeros(6);
    isStable = 0;
    
    % racunanje Jacobijeve matrike
    for i = 1:6
        for j = 1:6
            J(i, j) = X(i)*A(i, j);
        end
    end
    J = J - diag(diag(J)) + diag(A*X+b);
    
    % preverjanje ali je sistem stable
%     if  ~sum(real(eig(J)) > 0) % if stabilen
    if all(imag(eig(J)) < 1e-19 & imag(eig(J)) > -1e-19 ) % if ciklicen
        isStable = 1;
    end
end